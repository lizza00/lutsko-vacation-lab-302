<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <link rel="shortcut icon" href="images/beach.png" type="image/png">
    <link rel="stylesheet" href="css/back.css?3">
    <link rel="stylesheet" href="css/layout.css">
    <title>Vacation</title>
</head>
<body>
<t:layout>
    <sec:authorize access="isAuthenticated()">
        <p>Ваш логин: <sec:authentication property="principal.username" /></p>
        <p><a class="btn btn-lg btn-danger" href="logout" role="button">Выйти</a></p>

    </sec:authorize>
    <div class="photo-container" style="height: 90%;">
        <div class="left-title">Vacation is not a dream</div>
        <div class="right-title">anymore</div>
    </div>
    <div class="text_container">
        <div class="title">Bla-bla-bla</div>
        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
            dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
            deserunt mollit anim id est laborum.
        </div>
    </div>
    <div class="images-container">
        <div><img src="images/palm-tree.png" alt="palm-tree"></div>
        <div><img src="images/sea-lion.png" alt="sea-lion"></div>
        <div><img src="images/leaf.png" alt="leaf"></div>
    </div>
</t:layout>
</body>
</html>
