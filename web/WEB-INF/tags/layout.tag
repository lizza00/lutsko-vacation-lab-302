<%@tag description="Page layout tag" pageEncoding="UTF-8" %>
<div class="nav">
    <a href="/servlet_task_war/">
        Home
    </a>
    <a href="/servlet_task_war/list/1">
        Journeys
    </a>
    <a href="/servlet_task_war/create">
        New Journey
    </a>
</div>
<jsp:doBody/>
<div class="footer">
   <span>
      Liza Lutsko &mdash; 2019
   </span>
</div>