<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/error.css">
    <title>Exception</title>
</head>
<body>
<h2>Resource is unavailable</h2>
<div style="text-align: center">
    <p>${message}</p>
</div>
</body>
</html>
