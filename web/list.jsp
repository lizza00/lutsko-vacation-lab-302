<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="shortcut icon" href="images/beach.png" type="image/png">
    <link rel="stylesheet" href="css/search.css?1">
    <link rel="stylesheet" href="css/back.css?3">
    <link rel="stylesheet" href="css/table.css?3">
    <link rel="stylesheet" href="css/layout.css">
    <title>Journeys</title>
</head>
<body>
<t:layout>
    <div class="photo-container">

        <div align="center">
            <form method="get" action="" style="margin-block-end: 0;">
                <div class="row search">
                    <input type="text"  id="searchValue" name="searchValue" placeholder="Search.."
                          >
                    <input type="submit" value="Search">
                </div>

            </form>
        </div>
        <c:choose>
        <c:when test="${validationMessage != null}">
            <div class="validation">
                    ${validationMessage}</div>
        </c:when>
        <c:otherwise>
        <div class="table-container">
            <table>
                <thead>
                <tr>
                    <th> Name</th>
                    <th> Country</th>
                    <th> Cost</th>
                    <th> Duration</th>
                    <th> Description</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="tour" items="${tourList}">
                    <tr>
                        <td>${tour.getName()}</td>
                        <td>${tour.getCountry().getName()}</td>
                        <td>$${tour.getCost()}</td>
                        <td>${tour.getDuration()}</td>
                        <td>
                                ${ tour.getDescription()}...
                            <a href="/servlet_task_war/details/${tour.getId()}">
                                <div style="color:#043725;">See details</div>
                            </a>
                        </td>
                        <td>
                            <a href="/servlet_task_war/update/${tour.getId()}">
                                <img src="images/edit.png" width="30px" height="30px" alt="edit tour">
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            </c:otherwise>
            </c:choose>

            <c:if test="${numberOfPage > 0}">
                <div class="more" style="margin-left: 5%">
                    <a href="list/${numberOfPage - 1}"> See previous</a>
                </div>
            </c:if>

            <c:if test="${tourList.size() == 10}">
                <div class="more">
                    <a href="list/${numberOfPage + 1}"> See more</a>
                </div>
            </c:if>

        </div>
    </div>
</t:layout>
</body>
</html>
