package com.mykolayiv.basecamp.servlets.aspect;

import lombok.extern.log4j.Log4j;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@LogTime
@Interceptor
@Log4j
public class LogTimeInterceptor {
    public static boolean calledBefore = false;

    @AroundInvoke
    public Object auditMethod(InvocationContext ctx) throws Exception {
        if (!calledBefore) {
            calledBefore = true;
            log.info("Start of " + ctx.getMethod().getName() + " by " + ctx.getClass().getName());
        }

        Object result = ctx.proceed();
        if (calledBefore) {
            calledBefore = false;
            log.info("End of " + ctx.getMethod().getName() + " by " + ctx.getClass().getName());

        }
        return result;
    }
}