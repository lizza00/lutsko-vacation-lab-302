package com.mykolayiv.basecamp.servlets.publisher;

import com.mykolayiv.basecamp.servlets.controller.calculator.soap.api.v1.CalculatorControllerImpl;

import javax.xml.ws.Endpoint;

public class SoapPublisher{

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/calculator", new CalculatorControllerImpl());
    }

}