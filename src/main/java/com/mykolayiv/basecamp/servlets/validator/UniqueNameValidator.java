package com.mykolayiv.basecamp.servlets.validator;

import com.mykolayiv.basecamp.servlets.dao.TourDao;
import com.mykolayiv.basecamp.servlets.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.sql.SQLException;

@Component
public class UniqueNameValidator implements ConstraintValidator<UniqueName, String> {


    @Autowired
    private TourService tourService;

    public boolean isValid(String name, ConstraintValidatorContext context) {
        return !tourService.isTourByName(name);
    }
}
