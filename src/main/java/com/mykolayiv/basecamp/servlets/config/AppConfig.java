package com.mykolayiv.basecamp.servlets.config;

import com.mykolayiv.basecamp.servlets.dao.UserDao;
import com.mykolayiv.basecamp.servlets.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class AppConfig {
    @Autowired
    private UserDao userDao;
    @Bean
    public UserDetailsService getUserDetailsService(){
        return new UserService(userDao);
    }
}
