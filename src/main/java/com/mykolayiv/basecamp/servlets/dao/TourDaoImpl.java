package com.mykolayiv.basecamp.servlets.dao;

import com.mykolayiv.basecamp.servlets.aspect.LogTime;
import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import lombok.extern.log4j.Log4j;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.ejb.Stateless;
import java.util.List;

@Log4j
@Stateless
@Repository
public class TourDaoImpl implements TourDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public TourDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @LogTime
    @Override
    public List<Tour> selectListOfToursByName(String searchValue, int shift, int count) {
        return sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t WHERE LOWER(name) LIKE :name ")
                .setParameter("name", "%" + searchValue.toLowerCase() + "%")
                .setFirstResult((shift - 1) * count)
                .setMaxResults(count)
                .list();
    }

    @LogTime
    @Override
    public List<Tour> selectListOfTours(int shift, int count) {
        return sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t ")
                .setFirstResult((shift - 1) * count)
                .setMaxResults(count)
                .getResultList();
    }

    @Override
    public List<Tour> selectListOfTours() {
        return sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t ")
                .getResultList();
    }

    @LogTime
    @Override
    public Tour selectTourById(int id) {
        return (Tour) sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t WHERE id=:id")
                .setParameter("id", id)
                .getResultList().get(0);
    }

    @LogTime
    @Override
    public boolean existsTourByNameAndCountry(String name, int countryId) {
        return sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t WHERE name = :name AND country = :country")
                .setParameter("name", name)
                .setParameter("country", countryId)
                .list().size() > 0;
    }

    @LogTime
    public boolean existsTourByName(String name) {
        return sessionFactory.getCurrentSession().createQuery("SELECT t FROM Tour t WHERE name = :name")
                .setParameter("name", name)
                .list().size() > 0;
    }

    @LogTime
    @Override
    public List<Country> selectListOfCountries() {

        return sessionFactory.getCurrentSession().createQuery("SELECT c FROM Country c")
                .list();
    }

    @LogTime
    @Override
    public void insertTour(Tour tour) {
        sessionFactory.getCurrentSession().persist(tour);
        log.info("Tour is inserted");
    }

    @LogTime
    @Override
    public void updateTour(Tour tour) {
        sessionFactory.getCurrentSession().update(tour);
        log.info("Tour is updated");
    }


}
