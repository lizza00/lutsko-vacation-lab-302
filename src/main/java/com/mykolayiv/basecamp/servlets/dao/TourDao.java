package com.mykolayiv.basecamp.servlets.dao;

import com.mykolayiv.basecamp.servlets.aspect.LogTime;
import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository
public interface TourDao {

    List<Tour> selectListOfToursByName(String searchValue, int shift, int count) ;

    List<Tour> selectListOfTours(int shift, int count);

    List<Tour> selectListOfTours();

    Tour selectTourById(int id);

    boolean existsTourByNameAndCountry(String name, int countryId);

    boolean existsTourByName(String name) ;

    List<Country> selectListOfCountries() ;

    void insertTour(Tour tour);

    void updateTour(Tour tour);
}
