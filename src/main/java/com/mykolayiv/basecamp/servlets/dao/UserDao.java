package com.mykolayiv.basecamp.servlets.dao;

import com.mykolayiv.basecamp.servlets.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao {
    User findByEmail(String email);
}
