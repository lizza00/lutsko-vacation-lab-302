package com.mykolayiv.basecamp.servlets.dao;

import com.mykolayiv.basecamp.servlets.entity.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.ejb.Remove;

@Repository
public class UserDaoImpl implements UserDao {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Remove
    public User findByEmail(String email) {
        return (User) sessionFactory.getCurrentSession().createQuery("SELECT u FROM User u WHERE email=:email")
                .setParameter("email", email)
                .getResultList().get(0);
    }

}
