package com.mykolayiv.basecamp.servlets.calculator;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import java.util.concurrent.Future;

@Stateless
@Asynchronous
public interface Calculator {

    Future<Double> calculate(int x1, int x2);
}
