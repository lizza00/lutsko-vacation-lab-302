package com.mykolayiv.basecamp.servlets.service;

import com.mykolayiv.basecamp.servlets.dao.TourDao;
import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Stateful
@StatefulTimeout(unit = TimeUnit.MINUTES, value = 20)
@Service
public class TourService {

    private final TourDao tourDao;

    @Autowired
    public TourService(TourDao tourDao) {
        this.tourDao = tourDao;
    }

    @Transactional(readOnly = true)
    public Tour getTourById(int id) {
        return tourDao.selectTourById(id);
    }

    @Transactional(readOnly = true)
    public List<Tour> getAllToursByName(String searchValue, int shift, int count) {
        return tourDao.selectListOfToursByName(searchValue, shift, count);
    }

    @Transactional(readOnly = true)
    public List<Tour> getAllTours(int shift, int count) {
        return tourDao.selectListOfTours(shift, count);
    }

    @Transactional(readOnly = true)
    public List<Tour> getAllTours() {
        return tourDao.selectListOfTours();
    }

    @Transactional(readOnly = true)
    public boolean isTourByNameAndCountry(String name, int countryId) {
        return tourDao.existsTourByNameAndCountry(name, countryId);
    }

    @Transactional(readOnly = true)
    public boolean isTourByName(String name) {
        return tourDao.existsTourByName(name);
    }

    @Transactional(readOnly = true)
    public List<Country> getAllCountries() {
        return tourDao.selectListOfCountries();
    }

    @Transactional
    public void insertTour(Tour tour) {
        tourDao.insertTour(tour);
    }

    @Transactional
    public void updateTour(Tour tour) {
        tourDao.updateTour(tour);
    }
}
