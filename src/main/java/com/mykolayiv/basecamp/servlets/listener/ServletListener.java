package com.mykolayiv.basecamp.servlets.listener;

import lombok.extern.log4j.Log4j;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
@Log4j
public class ServletListener implements ServletRequestListener {

    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        log.info("DEstroying servlet");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        log.info("Initialyzing servlet");
    }
}
