package com.mykolayiv.basecamp.servlets.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Builder
@Data
@Entity
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue
    @Column(name = "country_id")
    private int id;

    @Column(name = "country_name")
    private String name;

    @Column(name = "country_capital")
    private String capital;

    public Country() {
    }

    public Country(int id, String name, String capital) {
        this.id = id;
        this.name = name;
        this.capital = capital;
    }
}
