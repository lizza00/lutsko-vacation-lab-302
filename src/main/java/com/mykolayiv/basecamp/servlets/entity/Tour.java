package com.mykolayiv.basecamp.servlets.entity;


import com.mykolayiv.basecamp.servlets.validator.UniqueName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Setter
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@Entity
@Table(name = "tour")
public class Tour {

    public static final int MIN_DURATION_VALUE = 1;
    public static final int MAX_DURATION_VALUE = 99;
    public static final int MIN_COST_VALUE = 0;
    public static final int MAX_COST_VALUE = 100_000;
    public static final int MIN_DESCRIPTION_LENGTH = 100;
    public static final int MAX_DESCRIPTION_LENGTH = 100_000;

    @Id
    @GeneratedValue
    @Column(name = "tour_id")
    private int id;

    @NotNull
    @Column(name = "tour_name")
    private String name;


    @Size(min = MIN_DESCRIPTION_LENGTH, max = MAX_DESCRIPTION_LENGTH)
    @Column(name = "tour_description")
    private String description;

    @Min(MIN_COST_VALUE)
    @Max(MAX_COST_VALUE)
    @Column(name = "tour_cost")
    private double cost;

    @Min(MIN_DURATION_VALUE)
    @Max(MAX_DURATION_VALUE)
    @Column(name = "tour_duration")
    private int duration;

    @NotNull
    @ManyToOne
    @JoinColumn(referencedColumnName = "country_id", name = "tour_country_id")
    private Country country;

    public Tour() {
    }

    public Tour(String name, String description, double cost, int duration, Country country) {
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.duration = duration;
        this.country = country;
    }
}
