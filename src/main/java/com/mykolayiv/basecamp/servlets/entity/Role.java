package com.mykolayiv.basecamp.servlets.entity;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "role")
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue
    @Column(name =  "role_id")
    private Long id;

    @Column(name = "role_name")
    private String role;

    @Override
    public String getAuthority() {
        return role;
    }
}
