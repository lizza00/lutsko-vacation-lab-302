package com.mykolayiv.basecamp.servlets.controller.api.v1;

import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import com.mykolayiv.basecamp.servlets.service.TourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class TourController {

    private final TourService tourService;

    public TourController(TourService tourService) {
        this.tourService = tourService;
    }

    @GetMapping("/tours")
    public @ResponseBody
    List<Tour> getTours() {
        return tourService.getAllTours();
    }

    @GetMapping("/countries")
    public @ResponseBody
    List<Country> getCountries() {
        return tourService.getAllCountries();
    }
}
