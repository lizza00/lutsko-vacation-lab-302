package com.mykolayiv.basecamp.servlets.controller;

import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import com.mykolayiv.basecamp.servlets.service.TourService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.validation.Valid;
import java.util.List;

@Asynchronous
@Singleton
@Controller("/tours")
@Log4j
public class TourController {
    private static final String TOUR_PARAMETER = "tour";
    private static final String MESSAGE_PARAMETER = "message";
    private static final String VALIDATION_MESSAGE_PARAMETER = "validationMessage";
    private static final String COUNTRY_LIST_PARAMETER = "countryList";
    private static final String ERROR_MESSAGE = "message";

    private final TourService tourService;

    @Autowired
    public TourController(TourService tourService) {
        this.tourService = tourService;
    }


    @PostMapping("/create")
    protected String create(Model model, @Valid @ModelAttribute Tour tour, BindingResult result) {

        if (result.hasErrors()) {
            List<Country> countries = tourService.getAllCountries();
            model.addAttribute(COUNTRY_LIST_PARAMETER, countries);
            model.addAttribute(TOUR_PARAMETER, tour);
            return "/create.jsp";
        }
        tourService.insertTour(tour);

        return "redirect: /servlet_task_war/list/1";
    }

    @GetMapping("/create")
    protected String create(Model model) {
        List<Country> countries = tourService.getAllCountries();
        model.addAttribute(COUNTRY_LIST_PARAMETER, countries);
        return "/create.jsp";
    }

    @GetMapping("/details/{id}")
    protected String details(Model model, @PathVariable int id) {
        log.debug("get details");

        try {
            Tour tour = tourService.getTourById(id);
            model.addAttribute(TOUR_PARAMETER, tour);
        } catch (Exception e) {
            String message = "Cannot get tour";
            model.addAttribute(MESSAGE_PARAMETER, e);
            log.error(message, e);
            return "/error.jsp";
        }
        log.debug("end of get details");
        return "/details.jsp";
    }

    @PostMapping("/update/{id}")
    protected String update(Model model, @PathVariable int id, @ModelAttribute Tour tour) {
        tour.setId(id);
        tourService.updateTour(tour);
        return "redirect: /servlet_task_war/list/1";
    }

    @GetMapping("/update/{id}")
    protected String update(Model model, @PathVariable int id) {
        List<Country> countries = tourService.getAllCountries();
        model.addAttribute(COUNTRY_LIST_PARAMETER, countries);
        Tour tour = tourService.getTourById(id);
        model.addAttribute(TOUR_PARAMETER, tour);
        log.debug("get end update");
        return "/update.jsp";
    }

    @GetMapping("/list/{page}")
    protected String list(Model model, @PathVariable int page, @ModelAttribute("searchValue") String searchValue) {

        List<Tour> tours = (searchValue.isEmpty()) ? tourService.getAllTours((page > 0) ? page : 1, 10) :
                tourService.getAllToursByName(searchValue, (page > 0) ? page : 1, 10);
        model.addAttribute("tourList", tours);
        return "/list.jsp";
    }
}
