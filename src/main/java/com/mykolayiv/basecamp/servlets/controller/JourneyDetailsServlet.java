/*
package com.mykolayiv.basecamp.servlets.controller;

import com.mykolayiv.basecamp.servlets.dao.TourDao;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import com.mykolayiv.basecamp.servlets.service.TourService;
import lombok.extern.log4j.Log4j;
import org.jboss.weld.context.ejb.Ejb;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Log4j
@WebServlet(name = "JourneyDetailsServlet", urlPatterns = {"/tou"})
public class JourneyDetailsServlet extends HttpServlet {

    private static final String ID_PARAMETER = "id";
    private static final String TOUR_PARAMETER = "tour";
    private static final String MESSAGE_PARAMETER = "message";

    @Autowired
    private TourService tourService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.debug("get details");
        request.logout();
        Arrays.stream(request.getCookies())
                .peek(c->c.setMaxAge(0))
                .forEach(response::addCookie);
        int id = Integer.parseInt(request.getParameter(ID_PARAMETER));
        try {
            Tour tour = tourService.getById(id);
            request.setAttribute(TOUR_PARAMETER, tour);
        } catch (Exception e) {
            String message = "Cannot get tour";
            request.setAttribute(MESSAGE_PARAMETER, e);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        log.debug("end of get details");
        request.getRequestDispatcher("/details.jsp").forward(request, response);
    }
}
*/
