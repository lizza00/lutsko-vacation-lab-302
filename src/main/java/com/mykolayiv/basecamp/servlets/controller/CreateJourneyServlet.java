/*
package com.mykolayiv.basecamp.servlets.controller;

import com.mykolayiv.basecamp.servlets.dao.TourDao;
import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Log4j
@WebServlet(name = "CreateJourneyServlet", urlPatterns = {"/tours/create"})
@ServletSecurity(
        value = @HttpConstraint(
                rolesAllowed = {
                        "protected"
                }),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "GET", rolesAllowed = {
                        "protected"
                })
        })
public class CreateJourneyServlet extends HttpServlet {

    private static final String NAME_PARAMETER = "name";
    private static final String COST_PARAMETER = "cost";
    private static final String DURATION_PARAMETER = "duration";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String COUNTRY_PARAMETER = "country";
    private static final String VALIDATION_MESSAGE_PARAMETER = "validationMessage";
    private static final String COUNTRY_LIST_PARAMETER = "countryList";
    private static final String ERROR_MESSAGE = "message";

    private TourDao tourDao;

    @Autowired
    public void setTourDao(TourDao tourDao) {
        this.tourDao = tourDao;
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter(NAME_PARAMETER);
        double cost = Double.parseDouble(request.getParameter(COST_PARAMETER));
        int duration = Integer.parseInt(request.getParameter(DURATION_PARAMETER));
        String description = request.getParameter(DESCRIPTION_PARAMETER);
        int countryId = Integer.parseInt(request.getParameter(COUNTRY_PARAMETER));
        Tour tour = Tour.builder()
                .name(name)
                .cost(cost)
                .duration(duration)
                .country(Country.builder()
                        .id(countryId)
                        .build())
                .description(description)
                .build();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Tour>> constraintViolations = validator.validate(tour);
        try {
            if(!constraintViolations.isEmpty()){
                List<Country> countries = tourDao.selectListOfCountries();
                request.setAttribute(COUNTRY_LIST_PARAMETER, countries);
                request.setAttribute(NAME_PARAMETER, name);
                request.setAttribute(COST_PARAMETER, cost);
                request.setAttribute(DURATION_PARAMETER, duration);
                request.setAttribute(DESCRIPTION_PARAMETER, description);
                request.setAttribute(COUNTRY_PARAMETER, countryId);
                request.setAttribute(VALIDATION_MESSAGE_PARAMETER, constraintViolations);
                doGet(request, response);
            } else {
                tourDao.insertTour(tour);
            }
        } catch (Exception e) {
            String message = "Cannot create journey";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        response.sendRedirect("/servlet_task_war/tours?page=0");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            log.info(request.getUserPrincipal());
            List<Country> countries = tourDao.selectListOfCountries();
            request.setAttribute(COUNTRY_LIST_PARAMETER, countries);
        } catch (Exception e) {
            String message = "Cannot get countries";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        getServletContext().getRequestDispatcher("/create.jsp").forward(request, response);
    }
}
*/
