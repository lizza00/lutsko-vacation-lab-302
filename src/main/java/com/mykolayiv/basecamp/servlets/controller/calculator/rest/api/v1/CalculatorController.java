package com.mykolayiv.basecamp.servlets.controller.calculator.rest.api.v1;

import com.mykolayiv.basecamp.servlets.calculator.Calculator;
import com.mykolayiv.basecamp.servlets.calculator.Result;
import com.mykolayiv.basecamp.servlets.factory.CalculatorFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/api/v1/calculator")
public class CalculatorController {

    @GetMapping("/{operator}/{x1}/{x2}")
    public @ResponseBody
    Result calculate(@PathVariable String operator,
                     @PathVariable Integer x1,
                     @PathVariable Integer x2) throws InterruptedException, ExecutionException {
        Calculator calculator = CalculatorFactory.resolve(operator);
        Future<Double>res = calculator.calculate(x1,x2);
        while(!res.isDone()){
            Thread.sleep(1000);
        }
        return new Result(res.get());
    }
}
