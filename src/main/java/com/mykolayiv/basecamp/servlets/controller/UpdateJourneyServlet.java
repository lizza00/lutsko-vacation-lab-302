/*
package com.mykolayiv.basecamp.servlets.controller;

import com.mykolayiv.basecamp.servlets.dao.TourDao;
import com.mykolayiv.basecamp.servlets.entity.Country;
import com.mykolayiv.basecamp.servlets.entity.Tour;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Log4j
@WebServlet(name = "UpdateJourneyServlet", urlPatterns = {"/tours/update"})
@ServletSecurity(
        value = @HttpConstraint(
                rolesAllowed = {
                        "protected"
                }),
        httpMethodConstraints = {
                @HttpMethodConstraint(value = "GET", rolesAllowed = {
                        "protected"
                }), @HttpMethodConstraint(value = "POST", rolesAllowed = {
                "protected"
        })
        })
public class UpdateJourneyServlet extends HttpServlet {

    private static final String ID_PARAMETER = "id";
    private static final String NAME_PARAMETER = "name";
    private static final String COST_PARAMETER = "cost";
    private static final String DURATION_PARAMETER = "duration";
    private static final String DESCRIPTION_PARAMETER = "description";
    private static final String COUNTRY_PARAMETER = "country";
    private static final String ERROR_MESSAGE = "message";
    private static final String COUNTRY_LIST_PARAMETER = "countryList";
    private static final String TOUR_PARAMETER = "tour";

    private TourDao tourDao;

    @Autowired
    public void setTourDao(TourDao tourDao) {
        this.tourDao = tourDao;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter(ID_PARAMETER));
        String name = request.getParameter(NAME_PARAMETER);
        double cost = Double.parseDouble(request.getParameter(COST_PARAMETER));
        int duration = Integer.parseInt(request.getParameter(DURATION_PARAMETER));
        String description = request.getParameter(DESCRIPTION_PARAMETER);
        int countryId = Integer.parseInt(request.getParameter(COUNTRY_PARAMETER));
        Tour tour = Tour.builder()
                .id(id)
                .name(name)
                .cost(cost)
                .duration(duration)
                .country(Country.builder()
                        .id(countryId)
                        .build())
                .description(description)
                .build();

        try {
            tourDao.updateTour(tour);

        } catch (Exception e) {
            String message = "Bad connection";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        response.sendRedirect("/servlet_task_war/tours?page=0");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Arrays.stream(request.getCookies())
                .peek(c -> c.setMaxAge(1800))
                .forEach(response::addCookie);

        int id = Integer.parseInt(request.getParameter(ID_PARAMETER));
        try {
            List<Country> countries = tourDao.selectListOfCountries();
            request.setAttribute(COUNTRY_LIST_PARAMETER, countries);
            Tour tour = tourDao.selectTourById(id);
            request.setAttribute(TOUR_PARAMETER, tour);
        } catch (Exception e) {
            String message = "Cannot get countries";
            request.setAttribute(ERROR_MESSAGE, message);
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
            log.error(message, e);
        }
        log.debug("get end update");
        getServletContext().getRequestDispatcher("/update.jsp").forward(request, response);
    }
}
*/
