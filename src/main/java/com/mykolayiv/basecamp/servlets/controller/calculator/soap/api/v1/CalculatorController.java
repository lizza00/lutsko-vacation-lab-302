package com.mykolayiv.basecamp.servlets.controller.calculator.soap.api.v1;

import com.mykolayiv.basecamp.servlets.calculator.Result;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.concurrent.ExecutionException;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public interface CalculatorController {
    @WebMethod(action = "/test")
    Result calculate(@WebParam String operator,
                     @WebParam Integer x1,
                     @WebParam Integer x2) throws ExecutionException;
}
