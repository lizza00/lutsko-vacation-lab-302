package com.mykolayiv.basecamp.servlets.controller.calculator.soap.api.v1;

import com.mykolayiv.basecamp.servlets.calculator.Calculator;
import com.mykolayiv.basecamp.servlets.calculator.Result;
import com.mykolayiv.basecamp.servlets.factory.CalculatorFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.concurrent.ExecutionException;

@WebService(endpointInterface = "com.mykolayiv.basecamp.servlets.controller.calculator.soap.api.v1.CalculatorController")
public class CalculatorControllerImpl implements CalculatorController {

    @WebMethod(action = "/test")
    public Result calculate(@WebParam String operator,
                            @WebParam Integer x1,
                            @WebParam Integer x2) throws ExecutionException {
        Calculator calculator = CalculatorFactory.resolve(operator);
        Result result = null;
        try {
            result = new Result(calculator.calculate(x1, x2).get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }
}
