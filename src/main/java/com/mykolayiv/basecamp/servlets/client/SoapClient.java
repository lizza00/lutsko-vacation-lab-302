package com.mykolayiv.basecamp.servlets.client;

import com.mykolayiv.basecamp.servlets.controller.calculator.soap.api.v1.CalculatorController;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

public class SoapClient {

    public static void main(String[] args) throws Exception {

        URL url = new URL("http://localhost:9999/ws/calculator?wsdl");
        QName qname = new QName("http://v1.api.soap.calculator.controller.servlets.basecamp.mykolayiv.com/", "CalculatorControllerImplService");

        Service service = Service.create(url, qname);

        CalculatorController controller = service.getPort(CalculatorController.class);

        System.out.println(controller.calculate("+", 2, 3));
        System.out.println(controller.calculate("*", 2, 3));
        System.out.println(controller.calculate(":", 2, 3));
        System.out.println(controller.calculate("-", 2, 3));
    }
}