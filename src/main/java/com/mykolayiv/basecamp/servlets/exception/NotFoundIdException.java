package com.mykolayiv.basecamp.servlets.exception;

public class NotFoundIdException extends Exception{
    public NotFoundIdException(String message){
        super(message);
    }
}
