package com.mykolayiv.basecamp.servlets.factory;

import com.mykolayiv.basecamp.servlets.calculator.Calculator;

import javax.ejb.AsyncResult;

public class CalculatorFactory {

    public static Calculator resolve(String operator) {
        Calculator calculator;
        if (operator.equals(Operator.SUM.getValue())) {
            calculator = (x1, x2) -> new AsyncResult<>((double) x1 + x2);
        } else if (operator.equals(Operator.MINUS.getValue())) {
            calculator = (x1, x2) -> new AsyncResult<>((double) x1 - x2);
        } else if (operator.equals(Operator.MULTIPLY.getValue())) {
            calculator = (x1, x2) -> new AsyncResult<>((double) x1 * x2);
        } else {
            calculator = (x1, x2) -> {
                if (x2 == 0) return new AsyncResult<>((double)0);
                return new AsyncResult<>((double) x1 / x2);
            };
        }
        return calculator;
    }

    enum Operator {
        SUM("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE(":");

        private String value;

        Operator(String op) {
            value = op;
        }

        public String getValue() {
            return value;
        }
    }
}
