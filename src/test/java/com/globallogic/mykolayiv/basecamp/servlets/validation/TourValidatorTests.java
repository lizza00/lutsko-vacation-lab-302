package com.globallogic.mykolayiv.basecamp.servlets.validation;

import com.mykolayiv.basecamp.servlets.entity.Tour;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TourValidatorTests {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeClass
    public static void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterClass
    public static void close() {
        validatorFactory.close();
    }

    @Test
    public void shouldHaveNoViolations() {
        Tour tour = new Tour();
        tour.setCost(Tour.MIN_COST_VALUE);
        tour.setDuration(Tour.MIN_DURATION_VALUE);
        StringBuilder validDescription = new StringBuilder();
        validDescription.setLength(Tour.MIN_DESCRIPTION_LENGTH);
        tour.setDescription(validDescription.toString());
        Set<ConstraintViolation<Tour>> violations
                = validator.validate(tour);
        assertTrue("No violations should occur", violations.isEmpty());
    }

    @Test
    public void shouldHaveThreeViolations() {
        Tour tour = new Tour();
        tour.setCost(Tour.MIN_COST_VALUE - 1);
        tour.setDuration(Tour.MIN_DURATION_VALUE - 1);
        StringBuilder validDescription = new StringBuilder();
        validDescription.setLength(Tour.MIN_DESCRIPTION_LENGTH - 1);
        tour.setDescription(validDescription.toString());
        Set<ConstraintViolation<Tour>> violations
                = validator.validate(tour);
        assertEquals("3 validation errors should occur", violations.size(), 3);
    }
}
