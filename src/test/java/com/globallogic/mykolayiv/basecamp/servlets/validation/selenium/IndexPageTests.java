package com.globallogic.mykolayiv.basecamp.servlets.validation.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

public class IndexPageTests {
    private static final String URL_RESOURCE = "http://127.0.0.1:43305/servlet_task/";
    private WebDriver webDriver;

    @Before
    public void setUp() {


        System.setProperty("webdriver.chrome.driver","src/test/resources/com/globallogic/mykolayiv/basecamp/servlets/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get(URL_RESOURCE);


    }

    @After
    public void tearDown() {
        webDriver.quit();
    }

    @Test
    public void shouldGetValidTitle() {
        String title = "Vacation";
        assertEquals("Title should be " + title, title, webDriver.getTitle());
    }
}
