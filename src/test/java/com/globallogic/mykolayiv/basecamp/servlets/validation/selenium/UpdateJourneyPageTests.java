package com.globallogic.mykolayiv.basecamp.servlets.validation.selenium;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

public class UpdateJourneyPageTests {
    private static final String URL_RESOURCE = "http://127.0.0.1:43305/servlet_task/updateJourney?id=1";
    private static WebDriver webDriver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/com/globallogic/mykolayiv/basecamp/servlets/chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get(URL_RESOURCE);
    }

    @AfterClass
    public static void tearDown() {
        webDriver.quit();
    }

    @Test
    public void shouldGetValidTitle() {
        String title = "Update Journey";
        assertEquals("Title should be " + title, title, webDriver.getTitle());
    }

    @Test
    public void shouldGetValidTourName() {
        String input = "input";
        String name = "name";
        assertEquals("Expected and actual tags should be the same", input, webDriver.findElement(By.name(name)).getTagName());
    }
}
